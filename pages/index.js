import {
  Link,
  Container,
  Heading,
  Box,
  Button,
  List,
  ListItem,
  useColorModeValue,
  chakra,
  WrapItem,
  Wrap
} from '@chakra-ui/react'
import Paragraph from '../components/paragraph'
import { BioSection, BioYear } from '../components/bio'
import Layout from '../components/layouts/article'
import Section from '../components/section'
import { IoMail } from 'react-icons/io5'
import Image from 'next/image'

const ProfileImage = chakra(Image, {
  shouldForwardProp: prop => ['width', 'height', 'src', 'alt'].includes(prop)
})

const Home = () => (
  <Layout>
    <Container>
      <Box
        borderRadius="lg"
        mb={6}
        p={3}
        textAlign="center"
        bg={useColorModeValue('whiteAlpha.500', 'whiteAlpha.200')}
        css={{ backdropFilter: 'blur(10px)' }}
      >
        Hello, I&apos;m an indie front-end developer based in Vietnamese!
      </Box>

      <Box display={{ md: 'flex' }}>
        <Box flexGrow={1}>
          <Heading as="h2" variant="page-title">
            Dang Ngoc Anh
          </Heading>
          <p>Front-end Developer</p>
        </Box>
        <Box
          flexShrink={0}
          mt={{ base: 4, md: 0 }}
          ml={{ md: 6 }}
          textAlign="center"
        >
          <Box
            borderColor="whiteAlpha.800"
            borderWidth={2}
            borderStyle="solid"
            w="100px"
            h="100px"
            display="inline-block"
            borderRadius="full"
            overflow="hidden"
          >
            <ProfileImage
              src="/images/anhdn.jpg"
              alt="Profile image"
              borderRadius="full"
              width="100%"
              height="100%"
            />
          </Box>
        </Box>
      </Box>

      <Section delay={0.1}>
        <Heading as="h3" variant="section-title">
          Work
        </Heading>
        <Paragraph>
          My name is Dang Ngoc Anh. I&apos;m 23 years old.
          I graduated from FPT University with a degree in developer website.
          I&apos;m working as a Frontend Developer in Physcode company with over 1 years experience developing different websites,
          from landing pages to big projects, mostly using Wordpress, Php.
          Then I worked at company Dtx Asia and developed the company e-commerce websites deliver the best customer experience,
          mostly using Reactjs, Nextjs, Gatbyjs. I learned a lot while working here.
          My goal for the next two years is to be a Senior Frontend Developer. I&apos;m now ready for more challenges and this position really excites me
        </Paragraph>
      </Section>

      <Section delay={0.2}>
        <Heading as="h3" variant="section-title">
          Bio
        </Heading>
        <BioSection>
          <BioYear>10/1999</BioYear>
          Born in Nam Định, Vietnamese.
        </BioSection>
        <BioSection>
          <BioYear>01/2020</BioYear>
          Complete website development program in the FPT
        </BioSection>
        <BioSection>
          <BioYear>03/2020</BioYear>
          Worked at Physcode Việt
        </BioSection>
        <BioSection>
          <BioYear>04/2021</BioYear>
          Worked at Dtx Asia
        </BioSection>
      </Section>

      <Section delay={0.3}>
        <Heading as="h3" variant="section-title">
          Tech I&apos;ve worked with
        </Heading>
        <Box my={4}>
          <Wrap spacing={2}>
            <WrapItem>
              <Button colorScheme='twitter'>Reactjs</Button>
            </WrapItem>
            <WrapItem>
              <Button colorScheme='green'>Nextjs</Button>
            </WrapItem>
            <WrapItem>
              <Button colorScheme='red'>Php</Button>
            </WrapItem>
            <WrapItem>
              <Button colorScheme='purple'>Javascript</Button>
            </WrapItem>
          </Wrap>

        </Box>
      </Section>

      <Section delay={0.4}>
        <Heading as="h3" variant="section-title">
          Contact
        </Heading>
        <List>
          <ListItem>
            <Link href="mailto: anhdn1690s@gmail.com" target="_blank">
              <Button
                variant="ghost"
                colorScheme="teal"
                leftIcon={<IoMail />}
              >
                anhdn1690s@gmail.com
              </Button>
            </Link>
          </ListItem>
        </List>
      </Section>

      <Section delay={0.5}>
        <Heading as="h3" variant="section-title">
          I ♥
        </Heading>
        <Paragraph>
          Game, Travel, Machine Learning, Computer
        </Paragraph>
      </Section>



    </Container>
  </Layout>
)

export default Home
export { getServerSideProps } from '../components/chakra'
